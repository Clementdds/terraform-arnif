resource "aws_key_pair" "admin" {
   key_name   = "admin"
   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC0ADzxWKrJSYvE1IpTqM0bw9BuKHXPrQ2qVznf+VJu1BitDLY8uYNvOsnbpPZE2NG9uyPYGfsk80UsZj++XDTx6RfbC3uy+A9XNohkuWNlu3oOr8Bh4rso90o6LaYmbITsdj7oou8zFSZ6N53Ri0LQoBAbwY0+r2L237kmwHNlToAa7hHmlFWv15RGmYbozjsQ/P+RZiG7BhhaQKeRx4r2/qAOnRg0nPRReBk/vHeGac8sR0DXragheSmC4DEclm+g4we3BXs0qmj7D7hfrR/vYJKqVWzxh8MAj13neLJxBj4P4Nt4neWGe0JBGWPHGcP5tIqAUPBMigYsDCcv72LnSm/s1enVlPTR051WUhrWUzdIuy2UbVbzdKcD3jcnf/p2gaMkJpkqZiyyHITnX3Pv5+5fvl6hDA0iqntHkxdtSd14otPdQY+3vPUtCas4fZ0KU1/7ShZq6aDhOiv1QVNGcKEn4pyiTcD1O67RBAIhkUY2vxEhwmRxokfxfkCyiuFIViXceR4gBIkKkADclgXCB9+l2Zy7HOrv0ufjnf9To2Ga74UDUoJSet9fak276tARRHpf+0z2eXHKMel7CR/a9vpOHzvWDHyznYiuLAJXq6yegVAftVkut0iCMnj94VMkqax+Clgt3eQg1wxjZXR2z9TsYn1NgpioQVannx2Dfw== thomas.curti@epita.fr"
 }

//App
resource "aws_s3_bucket" "default" {
  acl     = "public-read"
}

resource "aws_s3_bucket_object" "default" {
  bucket        = aws_s3_bucket.default.id
  key           = "Website_working.zip"
  acl           = "public-read"
  source        = "Website_working.zip"
}

resource "aws_elastic_beanstalk_application_version" "default" {
  name        = "tf-app-beanstalk-asp"
  application = aws_elastic_beanstalk_application.default.name
  bucket      = aws_s3_bucket.default.id
  key         = aws_s3_bucket_object.default.id
}

resource "aws_elastic_beanstalk_application" "default" {
  name        = "tf-beanstalk-asp-app"
}

// IAM
resource "aws_iam_instance_profile" "default" {
  name = "tf-aws-elasticbeanstalk-instance_profile"
  role = aws_iam_role.role.name
}

resource "aws_iam_role" "role" {
  name = "tf-aws-elasticbeanstalk-ec2-role"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}


//Env
resource "aws_elastic_beanstalk_environment" "beanstalk-asp-env" {

  depends_on = [aws_elastic_beanstalk_application_version.default]

  name                = "tf-beanstalk-asp-env"
  application         = aws_elastic_beanstalk_application.default.name
  solution_stack_name = "64bit Windows Server 2019 v2.6.0 running IIS 10.0"

  setting {
    namespace = "aws:ec2:vpc"
    name      = "VPCId"
    value     = aws_vpc.vpc-asp.id
  }
  setting {
    namespace = "aws:ec2:vpc"
    name = "Subnets"
    value = "${aws_subnet.subnet-public-1-asp.id}, ${aws_subnet.subnet-public-2-asp.id}"
  }
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name = "IamInstanceProfile"
    value = aws_iam_instance_profile.default.name
  }
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name = "SecurityGroups"
    value = aws_security_group.ssh-allowed.id
  }
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name = "EC2KeyName"
    value = aws_key_pair.admin.id
  }
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name = "InstanceType"
    value = "t2.micro"
  }
  setting {
    namespace = "aws:elasticbeanstalk:environment"
    name = "ServiceRole"
    value = "aws-elasticbeanstalk-service-role"
  }
  setting {
    namespace = "aws:ec2:vpc"
    name = "ELBScheme"
    value = "public"
  }
  setting {
    namespace = "aws:ec2:vpc"
    name = "ELBSubnets"
    value = "${aws_subnet.subnet-public-1-asp.id}, ${aws_subnet.subnet-public-2-asp.id}"
  }
  setting {
    namespace = "aws:elasticbeanstalk:command"
    name = "BatchSize"
    value = "30"
  }
  setting {
    namespace = "aws:elasticbeanstalk:command"
    name = "BatchSizeType"
    value = "Percentage"
  }
  setting {
    namespace = "aws:autoscaling:asg"
    name = "Availability Zones"
    value = "Any 2"
  }
  setting {
    namespace = "aws:autoscaling:asg"
    name = "MinSize"
    value = "2"
  }
  setting {
      namespace = "aws:elasticbeanstalk:environment:process:default"
      name      = "HealthCheckPath"
      value     = "/"
  }

  // DB 1
  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name = "RDS_USERNAME"
    value = aws_db_instance.postgres.username
  }
  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name = "RDS_PASSWORD"
    value = aws_db_instance.postgres.password
  }
  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name = "RDS_DATABASE"
    value = aws_db_instance.postgres.name
  }
  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name = "RDS_HOSTNAME"
    value = aws_db_instance.postgres.endpoint
  }


  // DB 2
  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name = "RDS_USERNAME2"
    value = aws_db_instance.postgres_replicate.username
  }
  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name = "RDS_PASSWORD2"
    value = aws_db_instance.postgres.password
  }
  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name = "RDS_DATABASE2"
    value = aws_db_instance.postgres_replicate.name
  }
  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name = "RDS_HOSTNAME2"
    value = aws_db_instance.postgres_replicate.endpoint
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name = "ASPNETCORE_ENVIRONMENT"
    value = "Development"
  }
  
}

resource "null_resource" "deploy_app" {
  depends_on = [aws_elastic_beanstalk_application_version.default, aws_elastic_beanstalk_environment.beanstalk-asp-env]

  provisioner "local-exec" {
    command = "aws --region us-east-1 elasticbeanstalk update-environment --environment-name tf-beanstalk-asp-env --version-label tf-app-beanstalk-asp"
  }
}