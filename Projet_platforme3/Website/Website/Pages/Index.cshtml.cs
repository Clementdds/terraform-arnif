﻿using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Npgsql;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Website.DataAccess.EFModels;
using Website.DataAccess.Interfaces;
using Website.Dbo;

namespace Website.Pages
{
    public class IndexModel : PageModel
    {
        [BindProperty(SupportsGet = true)]
        [Required]
        public string message { get; set; }

        [BindProperty(SupportsGet = true)]
        [Required]
        public string username { get; set; }

        private readonly IMessageRepository messageRepository_;
        public List<Message> Messages;

        public IndexModel(IMessageRepository messageRepository)
        {
            messageRepository_ = messageRepository;
            Messages = new List<Message>();

            message = "";
            username = "";
        }

        public async Task OnGet()
        {
            /*string conn = Startup.GetRDSConnectionString();
            NpgsqlConnection connection = new NpgsqlConnection(conn);
            try
            {
                connection.Open();
            }
            catch (Exception e)
            {
                try
                {
                    conn = Startup.GetRDSConnectionString(true);
                    connection = new NpgsqlConnection(conn);
                    connection.Open();
                }
                catch (Exception e2)
                {
                    Environment.Exit(1);
                    throw new Exception("Connection String: " + Startup.GetRDSConnectionString() + "\n\n" + e.Message + "\n\n" + e2.Message);
                }
            }

            var messages = await messageRepository_.Get();

            if (messages == null)
            {
                using (var cmd = new NpgsqlCommand("DROP TABLE IF EXISTS messages; CREATE TABLE IF NOT EXISTS messages(id serial PRIMARY KEY,username VARCHAR(255),body TEXT);", connection))
                {
                    cmd.ExecuteNonQuery();
                }
                messages = await messageRepository_.Get();

                if (messages == null)
                    Environment.Exit(1);
            }*/



            string connectionStringMaster = Startup.GetRDSConnectionString();
            string connectionStringReplicate = Startup.GetRDSConnectionString(true);
            bool isMasterUp = true;

            try
            {
                NpgsqlConnection conn = new NpgsqlConnection(connectionStringMaster);
                conn.Open();
            }
            catch (Exception e)
            {
                isMasterUp = false;
            }

            if (!isMasterUp)
            {
                connectionStringMaster = connectionStringReplicate;
            }

            NpgsqlConnection connection = new NpgsqlConnection(connectionStringMaster);
            connection.Open();

            try
            {
                using (var cmd = new NpgsqlCommand("SELECT * FROM messages;", connection))
                {
                    var reader = cmd.ExecuteReader();
                    Messages = new List<Message>();
                    while (reader.Read())
                    {
                        var mess = new Message()
                        {
                            Id = (int)reader[0],
                            Username = reader[1].ToString(),
                            Body = reader[2].ToString()
                        };
                        Messages.Add(mess);
                    }
                }
            }
            catch
            {
                using (var cmd = new NpgsqlCommand("CREATE TABLE IF NOT EXISTS messages(id serial PRIMARY KEY,username VARCHAR(255),body TEXT);", connection))
                {
                    cmd.ExecuteNonQuery();
                }
            }

            Messages.Reverse();
        }

        public async Task<IActionResult> OnPost()
        {
            /*Message message = new Message()
            {
                Body = this.message,
                Username = this.username + " le " + DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss")
            };

            await messageRepository_.Insert(message);*/

            string connectionStringMaster = Startup.GetRDSConnectionString();
            string connectionStringReplicate = Startup.GetRDSConnectionString(true);
            bool isMasterUp = true;

            try
            {
                NpgsqlConnection conn = new NpgsqlConnection(connectionStringMaster);
                conn.Open();
            }
            catch (Exception e)
            {
                isMasterUp = false;
            }

            if (!isMasterUp)
            {
                connectionStringMaster = connectionStringReplicate;
            }

            NpgsqlConnection connection = new NpgsqlConnection(connectionStringMaster);
            connection.Open();

            this.username = this.username + " le " + DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss");

            this.username = this.username.Replace("\'", " ");
            this.message = this.message.Replace("\'", " ");

            using (var cmd = new NpgsqlCommand($"INSERT INTO messages (username, body) VALUES('{this.username}', '{this.message}')", connection))
            {
                var reader = cmd.ExecuteReader();
                Messages = new List<Message>();
                while (reader.Read())
                {
                    var mess = new Message()
                    {
                        Id = (int)reader[0],
                        Username = reader[1].ToString(),
                        Body = reader[2].ToString()
                    };
                    Messages.Add(mess);
                }
            }

            return Redirect(Request.GetDisplayUrl());
        }
    }
}
