﻿using System;
using System.Collections.Generic;

namespace Website.DataAccess.EFModels
{
    public partial class Messages
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Body { get; set; }
    }
}
