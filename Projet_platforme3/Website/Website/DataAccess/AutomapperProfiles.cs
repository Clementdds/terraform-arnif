﻿using AutoMapper;

namespace Website.DataAccess
{
    public class AutomapperProfiles : Profile
    {
        public AutomapperProfiles()
        {
            CreateMap<Dbo.Message, EFModels.Messages>();
            CreateMap<EFModels.Messages, Dbo.Message>();
        }
    }
}
