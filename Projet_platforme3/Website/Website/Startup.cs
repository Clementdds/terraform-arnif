using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Website.DataAccess;
using Website.DataAccess.EFModels;
using Website.DataAccess.Interfaces;
using System.Configuration;
using System;
using Npgsql;

namespace Website
{
    public class Startup
    {
        private string connectionString_;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }


        public static string GetRDSConnectionString(bool getOther = false)
        {
            //var appConfig = ConfigurationManager.AppSettings;

            string dbname = Environment.GetEnvironmentVariable("RDS_DATABASE");
            if (string.IsNullOrEmpty(dbname) || dbname == "" || getOther)
                dbname = Environment.GetEnvironmentVariable("RDS_DATABASE2");

            string username = Environment.GetEnvironmentVariable("RDS_USERNAME");
            if (username == null || username == "" || username == string.Empty || getOther)
                username = Environment.GetEnvironmentVariable("RDS_USERNAME2");

            string password = Environment.GetEnvironmentVariable("RDS_PASSWORD");
            if (password == null || password == "" || password == string.Empty || getOther)
                password = Environment.GetEnvironmentVariable("RDS_PASSWORD2");

            string hostname = Environment.GetEnvironmentVariable("RDS_HOSTNAME").Split(":")[0];
            if (hostname == null || hostname == "" || hostname == string.Empty || getOther)
                hostname = Environment.GetEnvironmentVariable("RDS_HOSTNAME2").Split(":")[0];

            string port = Environment.GetEnvironmentVariable("RDS_HOSTNAME").Split(":")[1];
            if (port == null || port == "" || port == string.Empty || getOther)
                port = Environment.GetEnvironmentVariable("RDS_HOSTNAME2").Split(":")[1];

            return "Host=" + hostname + ";Port=" + port + ";Database=" + dbname + ";User ID=" + username + ";Password=" + password + ";";
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var conn = Startup.GetRDSConnectionString();
            NpgsqlConnection connection = new NpgsqlConnection(conn);
            try
            {
                connection.Open();
            }
            catch (Exception e)
            {
                try
                {
                    conn = Startup.GetRDSConnectionString(true);
                    connection = new NpgsqlConnection(conn);
                    connection.Open();
                }
                catch (Exception e2)
                {
                    Environment.Exit(1);
                    throw new Exception("Connection String: " + conn + "\n\n" + e.Message + "\n\n" + e2.Message);
                }
            }

            services.AddEntityFrameworkNpgsql()
                .AddDbContext<arnifContext>(options => options.UseNpgsql(conn), ServiceLifetime.Transient);

            services.AddAutoMapper(typeof(AutomapperProfiles));
            services.AddRazorPages();
            services.AddTransient<IMessageRepository, MessageRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            connectionString_ = Configuration.GetConnectionString("Postgres");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                //app.UseHsts();
            }

            //app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });
        }
    }
}
