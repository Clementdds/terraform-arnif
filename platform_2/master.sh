#!/bin/sh
echo "*****************************************"
echo " Installing PostgreSQL"
echo "*****************************************"
sudo yum -y install postgresql postgresql-server postgresql-devel postgresql-contrib postgresql-docs
sudo service postgresql initdb
sudo sed -i.bak -e 's/ident$/md5/' -e 's/peer$/md5/' /var/lib/pgsql9/data/pg_hba.conf
sudo /sbin/chkconfig --levels 235 postgresql on
sudo service postgresql start

sudo yum install -y vim

sudo yum install -y git

sudo su - postgres -c "git clone https://gitlab.com/Faltren/arnif.git"

sudo su - postgres -c "echo 'host postgres postgres 0.0.0.0/0 trust' >> data/pg_hba.conf"
sudo su - postgres -c "echo 'host all all 0.0.0.0/0 trust' >> data/pg_hba.conf"
sudo su - postgres -c "mv arnif/postgresconf_master data/postgresql.conf"

sudo service postgresql restart

sudo su - postgres -c "psql -f arnif/schema.sql"

sudo su - postgres -c "psql -c \"CREATE ROLE replicator REPLICATION LOGIN PASSWORD 'password'\""
sudo su - postgres -c "psql -c \"CREATE PUBLICATION bpub FOR TABLE messages\""
sudo su - postgres -c "psql -c \"GRANT ALL ON messages TO replicator\""

#clone directly in slave

#sleep 10 # to let slave be initialized

#sudo su - postgres -c "pg_dump postgres -t messages -s | psql postgres -h ${ip_slave}"