
resource "aws_elb" "loadbalancer-asp" {
  name               = "tf-loadbalancer-asp"
  subnets = [ aws_subnet.subnet-public-1-asp.id, aws_subnet.subnet-public-2-asp.id ]

  security_groups = [ aws_security_group.ssh-allowed.id ]

  listener {
    instance_port     = 5000
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 5
    timeout             = 3
    target              = "HTTP:5000/"
    interval            = 30
  }

  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400
}

output "elb_dns_name" {
  value       = aws_elb.loadbalancer-asp.dns_name
  description = "The domain name of the load balancer"
}