resource "aws_key_pair" "admin" {
   key_name   = "admin"
   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC0ADzxWKrJSYvE1IpTqM0bw9BuKHXPrQ2qVznf+VJu1BitDLY8uYNvOsnbpPZE2NG9uyPYGfsk80UsZj++XDTx6RfbC3uy+A9XNohkuWNlu3oOr8Bh4rso90o6LaYmbITsdj7oou8zFSZ6N53Ri0LQoBAbwY0+r2L237kmwHNlToAa7hHmlFWv15RGmYbozjsQ/P+RZiG7BhhaQKeRx4r2/qAOnRg0nPRReBk/vHeGac8sR0DXragheSmC4DEclm+g4we3BXs0qmj7D7hfrR/vYJKqVWzxh8MAj13neLJxBj4P4Nt4neWGe0JBGWPHGcP5tIqAUPBMigYsDCcv72LnSm/s1enVlPTR051WUhrWUzdIuy2UbVbzdKcD3jcnf/p2gaMkJpkqZiyyHITnX3Pv5+5fvl6hDA0iqntHkxdtSd14otPdQY+3vPUtCas4fZ0KU1/7ShZq6aDhOiv1QVNGcKEn4pyiTcD1O67RBAIhkUY2vxEhwmRxokfxfkCyiuFIViXceR4gBIkKkADclgXCB9+l2Zy7HOrv0ufjnf9To2Ga74UDUoJSet9fak276tARRHpf+0z2eXHKMel7CR/a9vpOHzvWDHyznYiuLAJXq6yegVAftVkut0iCMnj94VMkqax+Clgt3eQg1wxjZXR2z9TsYn1NgpioQVannx2Dfw== thomas.curti@epita.fr"
 }

data "template_file" "init_website" {
  template = file("run_site.tpl")

  vars = {
    ip_db1 = aws_instance.db1.private_ip
    ip_db2 = aws_instance.db2.private_ip
  }
}

output "db1_output" {
  value = aws_instance.db1.public_ip
}

output "db1_output_private" {
  value = aws_instance.db1.private_ip
}

resource "aws_launch_configuration" "launch-conf-asp" {
  name   = "tf-launch-config"
  image_id      = "ami-0652d6c7a2e2d090a" //"ami-0885b1f6bd170450c" //.NET Core "ami-0a28a1ad6f52a7332"
  instance_type = "t2.medium"
  user_data = data.template_file.init_website.rendered 
  key_name = "admin"

  security_groups = [ aws_security_group.ssh-allowed.id ]

  lifecycle {
    create_before_destroy = true
  }
}