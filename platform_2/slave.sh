#!/bin/bash
echo "*****************************************"
echo " Installing PostgreSQL"
echo "*****************************************"
sudo yum -y install postgresql postgresql-server postgresql-devel postgresql-contrib postgresql-docs
sudo service postgresql initdb
sudo sed -i.bak -e 's/ident$/md5/' -e 's/peer$/md5/' /var/lib/pgsql9/data/pg_hba.conf
sudo /sbin/chkconfig --levels 235 postgresql on
sudo service postgresql start

sudo yum install -y git

sudo su - postgres -c "git clone https://gitlab.com/Faltren/arnif.git"

sudo su - postgres -c "echo 'host all all 0.0.0.0/0 trust' >> data/pg_hba.conf"
sudo su - postgres -c "mv arnif/postgresconf_master data/postgresql.conf"

sudo service postgresql restart

sudo su - postgres -c "git clone git@gitlab.com:Faltren/arnif.git"
sudo su - postgres -c "psql -f arnif/schema.sql"

sleep 40 #to let master be initialized

sudo su - postgres -c "psql -c \"CREATE SUBSCRIPTION busb CONNECTION 'dbname=postgres host=${ip_master} user=replicator' PUBLICATION bpub\""