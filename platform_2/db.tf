data "template_file" "init_slave" {
  template = file("slave.sh")

  vars = {
    ip_master = aws_instance.db1.private_ip
  }
}

resource "aws_instance" "db1" {
  security_groups = [ aws_security_group.security-ssh-db.id ]
  ami             = "ami-096fda3c22c1c990a"
  instance_type   = "t2.medium"
  key_name        = "admin"
  subnet_id = aws_subnet.subnet-private-1-asp.id
  user_data = file("master.sh")
}

resource "aws_instance" "db2" {
  security_groups = [ aws_security_group.security-ssh-db.id ]
  ami             = "ami-096fda3c22c1c990a"
  instance_type   = "t2.medium"
  key_name        = "admin"
  subnet_id = aws_subnet.subnet-private-2-asp.id
  user_data = data.template_file.init_slave.rendered
}