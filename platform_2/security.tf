//ASP WEBSITE
resource "aws_security_group" "ssh-allowed" {
    vpc_id = aws_vpc.vpc-asp.id
    
    egress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 5000
        to_port = 5000
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 5001
        to_port = 5001
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        Name = "ssh-allowed"
    }
}

//DB
resource "aws_security_group" "security-ssh-db" {
    vpc_id = aws_vpc.vpc-asp.id
    
    egress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        protocol = "tcp"
        security_groups = [ aws_security_group.ssh-allowed.id ]
        from_port = 5432
        to_port = 5432
    }

    ingress {
        protocol = "tcp"
        cidr_blocks = [aws_vpc.vpc-asp.cidr_block]
        from_port = 5432
        to_port = 5432
    }

    tags = {
        Name = "security-ssh-asp"
    }
}