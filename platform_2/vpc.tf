resource "aws_vpc" "vpc-asp" {

  cidr_block = "10.0.0.0/16"
  enable_dns_support = "true"       //gives an internal domain name
  enable_dns_hostnames = "true"     //gives an internal host name
  enable_classiclink = "false"
  instance_tenancy = "default"

  tags = {
    Name = "tf-vpc-asp"
  }
}


// Public route subnet 1
resource "aws_route_table_association" "crta-public-subnet-1-asp"{

    subnet_id = aws_subnet.subnet-public-1-asp.id
    route_table_id = aws_route_table.public-crt-asp.id
}

// Public route subnet 2
resource "aws_route_table_association" "crta-public-subnet-2-asp"{

    subnet_id = aws_subnet.subnet-public-2-asp.id
    route_table_id = aws_route_table.public-crt-asp.id
}


resource "aws_route_table" "public-crt-asp" {
    
    vpc_id = aws_vpc.vpc-asp.id

    route {
        cidr_block = "0.0.0.0/0" 
        gateway_id = aws_internet_gateway.igw-asp.id
    }
    
    tags = {
        Name = "tf-public-crt"
    }
}

resource "aws_internet_gateway" "igw-asp" {

    vpc_id = aws_vpc.vpc-asp.id

    tags = {
        Name = "tf-igw-asp"
    }
}

// Public subnet 1
resource "aws_subnet" "subnet-public-1-asp" {
    
    vpc_id = aws_vpc.vpc-asp.id
    cidr_block = "10.0.1.0/24"
    map_public_ip_on_launch = "true"
    availability_zone = "us-east-1a"

    tags = {
        Name = "tf-subnet-public-1-asp"
    }
}

// Public subnet 2
resource "aws_subnet" "subnet-public-2-asp" {
    
    vpc_id = aws_vpc.vpc-asp.id
    cidr_block = "10.0.2.0/24"
    map_public_ip_on_launch = "true"
    availability_zone = "us-east-1b"

    tags = {
        Name = "tf-subnet-public-2-asp"
    }
}


// Public route subnet 1
resource "aws_route_table_association" "crta-public-subnet-3-asp"{

    subnet_id = aws_subnet.subnet-private-1-asp.id
    route_table_id = aws_route_table.public-crt-asp.id
}

// Public route subnet 2
resource "aws_route_table_association" "crta-public-subnet-4-asp"{

    subnet_id = aws_subnet.subnet-private-2-asp.id
    route_table_id = aws_route_table.public-crt-asp.id
}

// Private subnet 1
resource "aws_subnet" "subnet-private-1-asp" {
    
    vpc_id = aws_vpc.vpc-asp.id
    cidr_block = "10.0.3.0/24"
    map_public_ip_on_launch = "true"
    availability_zone = "us-east-1a"

    tags = {
        Name = "tf-subnet-private-1-asp"
    }
}

// Private subnet 2
resource "aws_subnet" "subnet-private-2-asp" {
    
    vpc_id = aws_vpc.vpc-asp.id
    cidr_block = "10.0.4.0/24"
    map_public_ip_on_launch = "true"
    availability_zone = "us-east-1b"

    tags = {
        Name = "tf-subnet-private-2-asp"
    }
}